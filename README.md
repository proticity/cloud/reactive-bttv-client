# Reactive Better Twitch TV Client
This library provides a client for BTTV with an interface based on functional reactive streams, using Reactor. The
programming model allows for easy streaming of data into your application with non-blocking semantics.

## Adding the Library
Maven:
```xml
<dependencies>
    <dependency>
        <groupId>org.proticity</groupId>
        <artifactId>reactive-bttv-client</artifactId>
        <version>0.1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```

Gradle:
```groovy
dependencies {
    implementation 'com.proticity.bttv:reactive-bttv-client:0.1.1-SNAPSHOT'
}
```

### Snapshot Releases
To use the snapshot releases of the POM you will need to add the snapshot repository.

Maven:
```xml
<project>
    <repositories>
        <repository>
            <id>oss-snapshots</id>
            <name>Sonatype OSS Snapshots</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
</project>
```

Gradle:
```groovy
repositories {
    maven {
        url 'https://oss.sonatype.org/content/groups/public'
        mavenContent {
            snapshotsOnly()
        }
    }
    
    // Alternatively, if you use the Java-Modern plugin...
    ossrhSnapshots()
}
```

## Documentation
* [Latest JavaDocs](https://proticity.gitlab.io/cloud/reactive-bttv-client/apidocs)
* [Source XRef](https://proticity.gitlab.io/cloud/reactive-bttv-client/source)
* [Unit Test Reports](https://proticity.gitlab.io/cloud/reactive-bttv-client/unit-tests)
* [Integration Test Reports](https://proticity.gitlab.io/cloud/reactive-bttv-client/integration-tests)
* [Test Code Coverage](https://proticity.gitlab.io/cloud/reactive-bttv-client/coverage)
* [CheckStyle Report](https://proticity.gitlab.io/cloud/reactive-bttv-client/checkstyle)

### Creating a Client
```java
BttvClient client = BttvClient.create().connect();
```

Some options are available for advanced uses.
```java
BttvClient client = BttvClient.create()
    .restEndpoint("http://localhost:8080/api") // Override the REST API endpoint
    .webSocketEndpoint("ws://localhost:8081/ws") // Override the PubSub API endpoint
    .secure(false) // Force off HTTPS
    .objectMapper(new ObjectMapper()) // Override the Jackson ObjectMapper
    .connectionProvider(HttpRequests.get()) // Override Reactor-Netty HTTP connection provider
    .sslProviderBuilder(provider) // Override the SSL Provider Builder for HTTPS connections
    .meterRegistry(Metrics.globalRegistry) // The Micrometer meter registry to use.
    .connect();
```

### REST APIs
The client's `globalEmoteSet()` method will return a `Flux` of `Emote` objects representing BTTV's global emotes.

```java
// Print out all global emotes.
Flux<Emote> emotes = client.globalEmoteSet();
emotes.subscribe(emote -> {
    System.out.println("Emote Name: " + emote.getName());
    System.out.println("URL: " + emote.getUrl());
    System.out.println("URL (2x): " + emote.getUrl(2));
    System.out.println("URL (4x): " + emote.getUrl(4));
    System.out.println();
});

// Perform an action on the latest set of global emotes hourly.
Flux.interval(Duration.ofHours(1))
    .flatMap(client::globalEmoteSet)
    .subscribe(...);

// Get a map of emotes, keyed by their name.
Map<String, Emote> globalEmotes = client.globalEmoteSet().collectMap(Emote::getName).block();
```

Similarly, the global badge sets can be returned by invoking the `badges()` method. Note that this method returns a
`Mono` rather than a `Flux` of badges, and that the `BadgeCollection` object it returns includes both a map of badges
(keyed by their name) but also a map of usernames to a subset of those badge mappings for each user who has been
assigned a badge. This makes looking up badge information starting with the user (to see which badges they have) or the
badge (seeing a badge and finding it's image URL for example) equally simple.

```java
BadgeCollection badges = client.badges().block();

// List all users with the developer badge.
for (String username : badges.getBadges().get("developer").getAssignments()) {
    System.out.println(username);
}

// List badges belonging to NightBot.
for (Badge badge : badges.getAssignments().get("nightbot").values()) {
    System.out.println("Badge Name: " + badge.getName());
    System.out.println("Description: " + badge.getDescription());
    System.out.println("URL: " + badge.getUrl());
    System.out.println();
}
```

Information for a channel can be retrieved with the channel's name. Like `badges()` this method returns a `Mono`. The
result includes a map of emotes, keyed by their name, and a set of known bots for the channel.

```java
Channel channel = client.channel("CharmedBaryon").block();
Set<String> bots = channel.getBots();
Emote emote = channel.getEmote().get("baryonFail");
```

### PubSub APIs
BTTV uses a PubSub API to allow users to subscribe to channels for notifications, and broadcast themselves to other
users on a channel topic when they join a channel. Other users subscribed to a channel topic when one broadcasts will
receive the user's information, including personal emotes if they are a BTTV pro subscriber. This allows the application
to perform substitution of those personal emotes in the user's messages. Like the other APIs these are reactive and
therefore even operations like subscribing return a `Publisher` which must be subscribed to before the operation
happens.

```java
// "Join" a channel, i.e. subscribe to broadcasts in it.
Mono<Void> join = client.subscribeChannel("TwitchPresents");
join.block();

// Broadcast user "BotUser" to the channel.
Mono<Void> broadcast = client.broadcast("TwitchPresents", "BotUser");
broadcast.block();

// Every time a user broadcasts themselves, print their info and personal emotes.
// Limit this action to only broadcasts for one channel via filter.
client.users()
    .filter(lookup -> "TwitchPresents".equals(lookup.getChannel()))
    .map(lookup -> lookup.getUser())
    .doOnNext(user -> {
        System.out.println("Username: " + user.getUsername());
        System.out.println("Display Name: " + user.getDisplayName());
        System.out.println("Pro Subscriber: " + user.isPro());
        System.out.println("Personal Emotes:");
    }).flatMap(user -> user.getEmotes())
    }).subscribe(emote -> {
        System.out.println("\t" + emote.getName() + ":");
        System.out.println("\t\t1x Image URL: " + emote.getUrl());
        System.out.println("\t\t2x Image URL: " + emote.getUrl(2));
        System.out.println("\t\t4x Image URL: " + emote.getUrl(4));
    });
```
