/**
 * The BTTV client module.
 */
module org.proticity.bttv.client {
    exports org.proticity.bttv.client;

    opens org.proticity.bttv.client;

    requires io.netty.buffer;
    requires io.netty.codec.http;
    requires org.reactivestreams;
    requires reactor.core;
    requires reactor.netty;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jdk8;
    requires slf4j.api;
    requires micrometer.core;
    requires jsr305;
}