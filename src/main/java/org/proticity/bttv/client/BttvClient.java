package org.proticity.bttv.client;

import java.io.Closeable;
import java.io.IOException;
import java.time.Duration;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SynchronousSink;
import reactor.netty.Connection;
import reactor.netty.http.HttpResources;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.websocket.WebsocketInbound;
import reactor.netty.resources.ConnectionProvider;
import reactor.netty.tcp.SslProvider;

/**
 * A client used to interact with the Better Twitch TV REST and WebSocket APIs.
 *
 * <p>
 * This client supports both the BTTV REST APIs and its PubSub API. The REST APIs allow on-demand requests for BTTV
 * data about badges, global emotes, and channels (with channel emotes). The PubSub API is used to handle the personal
 * emotes supported by BTTV's pro tier. Since it would be too expensive to proactively find all possible personal emotes
 * for all users, even in a single channel, BTTV has users subscribe via WebSocket sessions to listen for announcements
 * of personal emote information in one or more channels the user is chatting in. When a user with personal emotes joins
 * the channel it broadcasts a message with his/her arrival and all subscribed users receive the emote information.
 * </p>
 *
 * <p>
 * PubSub APIs receive messages as an endless stream. REST APIs receive only a single result as a {@link Mono}, but by
 * flat-mapping an interval {@link Flux} to these calls they can be easily made into an endless stream of up-to-date
 * information.
 * </p>
 */
@ParametersAreNonnullByDefault
public class BttvClient implements Closeable {
    /**
     * Logger for the BTTV client.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BttvClient.class);

    /**
     * The default timeout for a dispose operation, in seconds.
     */
    private static final int DEFAULT_DISPOSE_TIMEOUT = 3;

    /**
     * A copy of the builder used to construct this client.
     */
    private BttvClientBuilder builder;

    /**
     * An HTTP client prepared for use for API calls.
     */
    private HttpClient httpClient;

    /**
     * The WebSocket connection used for the PubSub API.
     */
    private Mono<? extends Connection> pubSubConnection;

    /**
     * The stream of incoming user lookup messages from the PubSub API.
     */
    private Flux<UserLookup> userBroadcasts;

    /**
     * A timer for the time spent on HTTP API requests.
     */
    private Timer requestsTimer;

    /**
     * The number of errors generated from API requests.
     */
    private Counter requestErrorsCounter;

    /**
     * A timer for the time spent on sending messages to the WebSocket connection.
     */
    private Timer pubSubSendsTimer;

    /**
     * The number of messages received from the WebSocket connection.
     */
    private Counter pubSubReceivesCounter;

    /**
     * The number of errors handling incoming messages from the WebSocket connection.
     */
    private Counter pubSubReceivesErrorCounter;

    /**
     * Creates a new BTTV client.
     *
     * @param builder the {@link BttvClientBuilder} used to build the client.
     */
    protected BttvClient(final BttvClientBuilder builder) {
        this.builder = new BttvClientBuilder(builder);

        // Setup Jackson
        var module = new SimpleModule();
        module.addDeserializer(BadgeCollection.class, new BadgeCollection.BadgesDeserializer());
        module.addDeserializer(Channel.class, new Channel.ChannelDeserializer());
        module.addDeserializer(Emote.class, new Emote.EmoteDeserializer());
        module.addDeserializer(UserLookup.class, new UserLookup.UserLookupDeserializer());
        this.builder.objectMapper.registerModules(module, new Jdk8Module());

        // Setup metrics.
        requestsTimer = this.builder.meterRegistry.timer("proticity.bttv.requests",
                "uri", builder.restEndpoint);
        requestErrorsCounter = this.builder.meterRegistry.counter("proticity.bttv.requests.errors",
                "uri", builder.restEndpoint);
        pubSubSendsTimer = this.builder.meterRegistry.timer("proticity.bttv.pubsub.send",
                "uri", builder.pubSubEndpoint);
        pubSubReceivesCounter = this.builder.meterRegistry.counter("proticity.bttv.pubsub.recv",
                "uri", builder.pubSubEndpoint);
        pubSubReceivesErrorCounter = this.builder.meterRegistry.counter("proticity.bttv.pubsub.recv.errors",
                "uri", builder.pubSubEndpoint);

        // Setup the HTTP client.
        httpClient = createHttpClient().baseUrl(builder.restEndpoint);

        // Setup the PubSub connection.
        pubSubConnection = createHttpClient()
                .websocket()
                .uri(builder.pubSubEndpoint)
                .connect()
                .cache();
        userBroadcasts = pubSubConnection.map(connection -> (WebsocketInbound) connection.inbound())
                .flatMapMany(WebsocketInbound::receiveFrames)
                .filter(frame -> frame instanceof TextWebSocketFrame)
                .cast(TextWebSocketFrame.class)
                .handle((TextWebSocketFrame frame, SynchronousSink<UserLookup> sink) -> {
                    try {
                        var msg = builder.objectMapper.readValue(frame.text(), PubSubMessage.class);
                        if ("lookup_user".equals(msg.name)) {
                            var lookupMsg = builder.objectMapper.readValue(frame.text(), UserLookup.class);
                            sink.next(lookupMsg);
                        } else {
                            var text = frame.text()
                                    .replace("\n", "\\n").replace("\r", "\\r");
                            LOGGER.warn("Unknown/incompatible BTTV PubSub message received: {}", text);
                        }
                    } catch (JsonMappingException | JsonParseException e) {
                        sink.error(new BttvApiException("Incompatible PubSub message received.", e));
                    } catch (IOException e) {
                        sink.error(e);
                    }
                })
                .publish()
                .autoConnect()
                .doOnEach(signal -> pubSubReceivesCounter.increment())
                .doOnError(err -> pubSubReceivesErrorCounter.increment());
    }

    /**
     * Create a basic {@link HttpClient}, with security as per the builder specification.
     *
     * @return A new {@link HttpClient}.
     */
    protected HttpClient createHttpClient() {
        if (builder.secure) {
            if (builder.sslProviderBuilder == null) {
                return HttpClient.create();
            } else {
                return HttpClient.create().secure(builder.sslProviderBuilder);
            }
        }
        return HttpClient.create();
    }

    /**
     * Retrieves the latest badge information for BTTV.
     *
     * <p>
     * This will include the whole map of badge types which are available and the assignments of badges to users.
     * </p>
     *
     * @return The badge information for BTTV.
     */
    public Mono<BadgeCollection> badges() {
        return request(Mono.just("/badges"), BadgeCollection.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        dispose();
    }

    /**
     * Dispose of the client asynchronously.
     */
    public void dispose() {
        pubSubConnection.doOnNext(conn -> conn.dispose());
    }

    /**
     * Disposes of the client in a blocking fashion.
     */
    public void disposeNow() {
        disposeNow(Duration.ofSeconds(DEFAULT_DISPOSE_TIMEOUT));
    }

    /**
     * Disposes of the client in a blocking fashion.
     *
     * @param timeout the timeout period for waiting for the disposal to complete.
     */
    public void disposeNow(Duration timeout) {
        pubSubConnection.doOnNext(conn -> conn.disposeNow(timeout)).block(timeout);
    }

    /**
     * Retrieves the emote data for all BTTV global emotes.
     *
     * @return The emote data for all BTTV global emotes.
     */
    public Flux<Emote> globalEmoteSet() {
        return request(Mono.just("/emotes"), EmotesModel.class)
                .flatMapMany(model -> {
                    for (var emote : model.emotes) {
                        emote.urlTemplate = model.urlTemplate;
                    }
                    return Flux.fromIterable(model.emotes);
                });
    }

    /**
     * Retrieve the BTTV information for a given channel.
     *
     * <p>
     * This will include that channels known bots and the emotes available to chatters in the channel
     * </p>
     *
     * @param channel the channel whose information should be retrieved.
     *
     * @return The information for the given channel.
     */
    public Mono<Channel> channel(String channel) {
        return channel(Mono.just(channel));
    }

    /**
     * Retrieve the BTTV information for a given channel.
     *
     * <p>
     * This will include that channels known bots and the emotes available to chatters in the channel
     * </p>
     *
     * @param channel the channel whose information should be retrieved.
     *
     * @return The information for the given channel.
     */
    public Mono<Channel> channel(Mono<String> channel) {
        var cachedChannel = channel.cache();
        return request(channel.map(name -> "/channels/" + name), Channel.class)
                .doOnNext(chan -> {
                    chan.name = cachedChannel.block();
                });
    }

    /**
     * Perform a get request against the BTTV REST API.
     *
     * @param uri the URI to which to make the request.
     * @param clazz the class of the deserialized response type.
     * @param <T> the type of the deserialized response.
     *
     * @return the deserialized response.
     */
    protected <T> Mono<T> request(Mono<String> uri, Class<T> clazz) {
        long startTime = System.currentTimeMillis();
        return httpClient
                .get()
                .uri(uri)
                .response((response, content) -> {
                    if (response.status().code() >= HttpResponseStatus.BAD_REQUEST.code()) {
                        throw new BttvApiException("Error response " + response.status().code() +
                                " from BTTV API for request.");
                    }
                    return content.aggregate()
                            .asByteArray()
                            .map(input -> {
                                requestsTimer.record(System.currentTimeMillis() - startTime, TimeUnit.MILLISECONDS);
                                try {
                                    return builder.objectMapper.readValue(input, clazz);
                                } catch (IOException e) {
                                    throw new BttvApiException("Incompatible API response received.", e);
                                }
                            });
                })
                .doOnError(err -> requestErrorsCounter.increment())
                .single();
    }

    /**
     * Subscribe to notifications of user broadcasts to a given channel.
     *
     * @param channel the channel to which to subscribe.
     *
     * @return A {@link Mono} tracking completion of the subscription.
     */
    public Mono<Void> subscribeChannel(String channel) {
        var msg = new PubSubMessage("join_channel");
        var data = builder.objectMapper.createObjectNode();
        data.put("name", channel);
        msg.data = data;
        return sendPubSubMessage(msg);
    }

    /**
     * Unsubscribe from notifications for user information from a channel.
     *
     * @param channel the channel from which to unsubscribe.
     *
     * @return A {@link Mono} tracking completion of the unsubscription.
     */
    public Mono<Void> unsubscribeChannel(String channel) {
        var msg = new PubSubMessage("part_channel");
        var data = builder.objectMapper.createObjectNode();
        data.put("name", channel);
        msg.data = data;
        return sendPubSubMessage(msg);
    }

    /**
     * Tells BTTV to broadcast the information for a given user to a given channel.
     *
     * <p>
     * This can be used to ensure all BTTV users subscribed to the given channel (which BTTV does automatically while
     * viewing a given channel) receive a lookup message with that user's personal emotes. This lets BTTV substitute in
     * the user's personal emotes in their messages for other BTTV viewers.
     * </p>
     *
     * @param username the username whose information should be broadcast.
     * @param channel the channel to which to broadcast the information.
     *
     * @return A {@link Mono} representing completion of sending the message.
     */
    public Mono<Void> broadcast(String channel, String username) {
        var msg = new PubSubMessage("broadcast_me");
        var data = builder.objectMapper.createObjectNode();
        data.put("name", username.toLowerCase(Locale.ROOT));
        data.put("channel", channel);
        msg.data = data;
        return sendPubSubMessage(msg);
    }

    /**
     * Returns a stream of user lookup information from users who joined a channel the client is subscribed to.
     *
     * @return A stream of user lookup information from users who joined a channel the client is subscribed to.
     */
    public Flux<UserLookup> users() {
        return userBroadcasts;
    }

    /**
     * Sends a message to the BTTV PubSub API by serializing the provided object.
     *
     * @param message the object to serialize into a BTTV PubSub message.
     *
     * @return A {@link Mono} for the completion of sending the message.
     */
    private Mono<Void> sendPubSubMessage(Object message) {
        long startTime = System.currentTimeMillis();
        return pubSubConnection.flatMap(connection -> {
            try {
                return connection.outbound()
                        .sendString(Mono.just(builder.objectMapper.writeValueAsString(message)))
                        .then()
                        .doOnEach(signal -> pubSubSendsTimer.record(System.currentTimeMillis() - startTime,
                                TimeUnit.MILLISECONDS));
            } catch (JsonMappingException | JsonParseException e) {
                throw new BttvApiException("Message is not compatible with known BTTV PubSub message types.", e);
            } catch (IOException e) {
                throw new BttvApiException("I/O error while sending PubSub message.", e);
            }
        });
    }

    /**
     * Create a new builder for the {@link BttvClient}.
     *
     * @return A builder which has a fluent API for creating a client.
     */
    public static BttvClientBuilder create() {
        return new BttvClientBuilder();
    }

    /**
     * A builder interface which provides a fluent API for creating a new {@link BttvClient}.
     */
    public static final class BttvClientBuilder {
        /**
         * The {@link ConnectionProvider} to use for establishing HTTP connections.
         */
        ConnectionProvider connectionProvider = HttpResources.get();

        /**
         * The Jackson {@link ObjectMapper} to use to serialize and deserialize JSON data.
         */
        ObjectMapper objectMapper = new ObjectMapper();

        /**
         * The endpoint of the BTTV REST API.
         */
        private String restEndpoint = "https://api.betterttv.net/2/";

        /**
         * The endpoint of the BTTV WebSocket PubSub API.
         */
        private String pubSubEndpoint = "wss://sockets.betterttv.net/ws";

        /**
         * Whether or not to use a secure HTTPS/WSS connection; defaults to <code>true</code>.
         */
        private boolean secure = true;

        /**
         * The SSL provider builder to use to secure the BTTV client connection.
         */
        private Consumer<? super SslProvider.SslContextSpec> sslProviderBuilder;

        /**
         * The registry for metrics.
         */
        private MeterRegistry meterRegistry = Metrics.globalRegistry;

        /**
         * Creates a new {@link BttvClientBuilder}.
         */
        protected BttvClientBuilder() {
        }

        /**
         * Copy an existing {@link BttvClientBuilder}.
         *
         * @param builder an existing builder.
         */
        protected BttvClientBuilder(final BttvClientBuilder builder) {
            objectMapper = builder.objectMapper;
            connectionProvider = builder.connectionProvider;
            secure = builder.secure;
            sslProviderBuilder = builder.sslProviderBuilder;
            restEndpoint = builder.restEndpoint;
            pubSubEndpoint = builder.pubSubEndpoint;
            meterRegistry = builder.meterRegistry;
        }

        /**
         * Set the {@link ConnectionProvider} to use for establishing HTTP connections.
         *
         * @param connectionProvider the {@link ConnectionProvider} to use for establishing HTTP connections.
         *
         * @return A reference to this {@link BttvClientBuilder}.
         */
        public BttvClientBuilder connectionProvider(ConnectionProvider connectionProvider) {
            this.connectionProvider = connectionProvider;
            return this;
        }

        /**
         * Set the Jackson {@link ObjectMapper} to use to serialize and deserialize JSON data.
         *
         * @param objectMapper the {@link ObjectMapper} to use.
         *
         * @return A reference to this {@link BttvClientBuilder}.
         */
        public BttvClientBuilder objectMapper(ObjectMapper objectMapper) {
            this.objectMapper = objectMapper;
            return this;
        }

        /**
         * Sets the endpoint where the BTTV REST API is running.
         *
         * @param restEndpoint the endpointwhere the BTTV REST API is running.
         *
         * @return A reference to this {@link BttvClientBuilder}.
         */
        public BttvClientBuilder restEndpoint(String restEndpoint) {
            this.restEndpoint = restEndpoint;
            return this;
        }

        /**
         * Sets the endpoint where the BTTV WebSocket PubSub API is running.
         *
         * @param pubSubEndpoint the endpoint where the BTTV WebSocket PubSub API is running.
         *
         * @return A reference to this {@link BttvClientBuilder}.
         */
        public BttvClientBuilder pubSubEndpoint(String pubSubEndpoint) {
            this.pubSubEndpoint = pubSubEndpoint;
            return this;
        }

        /**
         * Enables secure connections for the BTTV client; this is enabled by default.
         *
         * @return A reference to this {@link BttvClientBuilder}.
         *
         * @see BttvClientBuilder#secure(boolean)
         * @see BttvClientBuilder#secure(Consumer)
         */
        public BttvClientBuilder secure() {
            return secure(true);
        }

        /**
         * Whether to use a secure connection to the BTTV API.
         *
         * <p>
         * If no custom SSL provider builder is provided, then this will use the default builder.
         * </p>
         *
         * @param secure whether to secure the connection to the BTTV API.
         *
         * @return A reference to this {@link BttvClientBuilder}.
         */
        public BttvClientBuilder secure(boolean secure) {
            this.secure = secure;
            if (!secure) {
                sslProviderBuilder = null;
            }
            return this;
        }

        /**
         * The SSL provider builder to use to secure the BTTV client connections.
         *
         * @param sslProviderBuilder A function which will build the SSL provider.
         *
         * @return A reference to this {@link BttvClientBuilder}.
         */
        public BttvClientBuilder secure(Consumer<? super SslProvider.SslContextSpec> sslProviderBuilder) {
            this.sslProviderBuilder = sslProviderBuilder;
            return secure(true);
        }

        /**
         * Sets the meter registry to use for metrics.
         *
         * @param meterRegistry the registry to which metrics are written.
         *
         * @return A reference to this {@link BttvClientBuilder}.
         */
        public BttvClientBuilder meterRegistry(MeterRegistry meterRegistry) {
            this.meterRegistry = meterRegistry;
            return this;
        }

        /**
         * Builds the {@link BttvClient} and configures the reactive streams for use.
         *
         * @return A new {@link BttvClient}.
         */
        public BttvClient connect() {
            return new BttvClient(this);
        }
    }
}
