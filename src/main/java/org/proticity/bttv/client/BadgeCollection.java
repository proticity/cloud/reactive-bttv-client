package org.proticity.bttv.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains information about the badges and badge assignments across BTTV at a given point in time.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class BadgeCollection implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * A mapping of badge names to the badge information.
     */
    private Map<String, Badge> badges;

    /**
     * A mapping of usernames to a map of badges they have been assigned.
     */
    private Map<String, Map<String, Badge>> badgeAssignments;

    /**
     * Create a new {@link BadgeCollection} collection.
     */
    protected BadgeCollection() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BadgeCollection badgeCollection = (BadgeCollection) o;
        return badges.equals(badgeCollection.badges);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(badges);
    }

    /**
     * Returns a mapping of badge names to the badge information.
     *
     * @return A mapping of badge names to the badge information.
     */
    @JsonGetter("badges")
    public Map<String, Badge> getBadges() {
        return badges;
    }

    /**
     * Returns a mapping of usernames to a map of badges they have been assigned.
     *
     * @return A mapping of usernames to a map of the badges they have been assigned.
     */
    @JsonIgnore
    public Map<String, Map<String, Badge>> getBadgeAssignments() {
        if (badgeAssignments == null) {
            badgeAssignments = new HashMap<>();
            for (var badge : badges.values()) {
                for (var user : badge.getAssignments()) {
                    var userBadgeMap = badgeAssignments.computeIfAbsent(user, key -> new HashMap<>());
                    userBadgeMap.put(badge.getName(), badge);
                }
            }
        }
        return badgeAssignments;
    }

    /**
     * A JSON deserializer for {@link BadgeCollection}.
     */
    static class BadgesDeserializer extends StdDeserializer<BadgeCollection> {
        private static final long serialVersionUID = 0;
        private static final Logger LOGGER = LoggerFactory.getLogger(BadgesDeserializer.class);

        /**
         * A default deserializer constructor.
         */
        BadgesDeserializer() {
            this(null);
        }

        /**
         * Constructs a new deserializer.
         */
        BadgesDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public BadgeCollection deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var badges = new BadgeCollection();

            var typesNode = rootNode.get("types");
            badges.badges = new HashMap<>(typesNode.size());
            for (var typeNode : typesNode) {
                var badge = new Badge();
                badge.url = typeNode.get("svg").textValue();
                badge.name = typeNode.get("name").textValue();
                badge.description = typeNode.get("description").textValue();
                badges.badges.put(badge.getName(), badge);
            }

            var assignmentsNode = rootNode.get("badges");
            badges.badgeAssignments = new HashMap<>(assignmentsNode.size());
            for (var assignmentNode : assignmentsNode) {
                var username = assignmentNode.get("name").textValue();
                var badgeName = assignmentNode.get("type").textValue();
                var badge = badges.badges.get(badgeName);
                if (badge == null) {
                    badgeName = badgeName.replace("\n", "\\n").replace("\r", "\\r");
                    username = username.replace("\n", "\\n").replace("\r", "\\r");
                    LOGGER.warn("Badge '{}' for user '{}' was not found in the badge types list.", badgeName, username);
                    continue;
                }

                var assignmentMap = badges.badgeAssignments.computeIfAbsent(username, key -> new HashMap<>());
                assignmentMap.put(badgeName, badge);
                badge.getAssignments().add(username);
            }

            return badges;
        }
    }
}
