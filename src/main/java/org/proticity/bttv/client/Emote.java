package org.proticity.bttv.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Represents data for a BTTV emote.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class Emote implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * The maximum size multiplier value for a BTTV emote.
     */
    private static final int MAX_EMOTE_SIZE = 3;

    /**
     * The name of the emote, i.e. the phrase which, when matched in a message, is replaced with this emote.
     */
    String name;

    /**
     * A unique ID for the emote.
     */
    String id;

    /**
     * The type of the emote's image data.
     */
    String imageType;

    /**
     * The owner of the emote, if it is not global.
     */
    String owner;

    /**
     * The template used to resolve the emote's URL.
     */
    String urlTemplate;

    /**
     * Constructs a new {@link Emote}.
     */
    protected Emote() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Emote emote = (Emote) o;
        return name.equals(emote.name) &&
                id.equals(emote.id) &&
                imageType.equals(emote.imageType) &&
                Objects.equals(owner, emote.owner) &&
                urlTemplate.equals(emote.urlTemplate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, id, imageType, owner, urlTemplate);
    }

    /**
     * Retrieves the URL for the emote in a given size.
     *
     * @param size the size multiplier for the emote. In BTTV this is 1 for the smallest size, 2 for medium, and 3 for
     *             large. BTTV does not interpret "4" as 4x the original size as Twitch does (only 3 works for this).
     *
     * @return The URL for the emote image in the given size.
     *
     * @see Emote#getUrl()
     */
    public String getUrl(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Size is a multiplier for the base size and must be greater than zero.");
        }
        return "https:" + urlTemplate.replace("{{id}}", getId()).replace("{{image}}",
                "" + Math.min(MAX_EMOTE_SIZE, size) + "x");
    }

    /**
     * Retrieves the URL for the emote in its base size (size 1).
     *
     * @return The URL for the emote in its base size (size 1).
     *
     * @see Emote#getUrl(int)
     */
    @JsonIgnore
    public String getUrl() {
        return getUrl(1);
    }

    /**
     * Returns the template for URLs.
     *
     * @return The template for URLs.
     */
    @JsonGetter("urlTemplate")
    protected String getUrlTemplate() {
        return urlTemplate;
    }

    /**
     * Returns the name of the emote, i.e. the phrase which, when matched in a message, is replaced with this emote.
     *
     * @return The name of the emote, i.e. the phrase which, when matched in a message, is replaced with this emote.
     */
    @JsonGetter("name")
    public String getName() {
        return name;
    }

    /**
     * Returns the unique ID for the emote.
     *
     * @return The unique ID for the emote.
     */
    @JsonGetter("id")
    public String getId() {
        return id;
    }

    /**
     * Returns the type of the emote's image data.
     *
     * @return The type of the emote's image data.
     */
    @JsonGetter("imageType")
    public String getImageType() {
        return Objects.requireNonNullElse(imageType, "png");
    }

    /**
     * Returns the owner of the emote, if it is not global.
     *
     * @return The owner of the emote, if it is not global.
     */
    @JsonGetter("owner")
    public Optional<String> getOwner() {
        return Optional.ofNullable(owner);
    }

    /**
     * Returns whether or not this is a global emote.
     *
     * A global emote is one which does not have an owner.
     *
     * @return Whether or not this is a global emote.
     */
    @JsonGetter("global")
    public boolean isGlobal() {
        return owner == null;
    }

    /**
     * A JSON deserializer for the {@link Emote} class.
     */
    static class EmoteDeserializer extends StdDeserializer<Emote> {
        private static final long serialVersionUID = 0;

        /**
         * Create a default {@link EmoteDeserializer}.
         */
        EmoteDeserializer() {
            this(null);
        }

        /**
         * Constructs a new deserializer.
         */
        EmoteDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Emote deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var emote = new Emote();

            emote.id = rootNode.get("id").textValue();
            emote.name = rootNode.get("code").textValue();
            emote.imageType = rootNode.get("imageType").textValue();

            var node = rootNode.get("urlTemplate");
            if (node != null) {
                emote.urlTemplate = node.textValue();
            }

            node = rootNode.get("channel");
            if (node != null) {
                emote.owner = node.textValue();
            }

            return emote;
        }
    }
}
