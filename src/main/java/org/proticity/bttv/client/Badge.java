package org.proticity.bttv.client;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Represents information about a BTTV badge.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class Badge implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * A unique name for the badge.
     */
    String name;

    /**
     * A description of the badge.
     */
    String description;

    /**
     * The URL of the SVG image for the badge.
     */
    String url;

    /**
     * The set of users to whom the badge is assigned.
     */
    private Set<String> assignments;

    /**
     * Constructs a new {@link Badge}.
     */
    Badge() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Badge badge = (Badge) o;
        return Objects.equals(name, badge.name) &&
                Objects.equals(description, badge.description) &&
                Objects.equals(url, badge.url);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, description, url);
    }

    /**
     * Returns the unique name for the badge.
     *
     * @return The unique name for the badge.
     */
    @JsonGetter("name")
    public String getName() {
        return name;
    }

    /**
     * The description of the badge.
     *
     * @return The description of the badge.
     */
    @JsonGetter("description")
    public String getDescription() {
        return description;
    }

    /**
     * Returns the URL of the SVG image for the badge.
     *
     * @return The URL of the SVG image for the badge.
     */
    @JsonGetter("url")
    public String getUrl() {
        return url;
    }

    /**
     * Returns the set of users to whom the badge has been assigned.
     *
     * @return The set of users to whom the badge has been assigned.
     */
    @JsonGetter("assignments")
    public Set<String> getAssignments() {
        if (assignments == null) {
            assignments = new HashSet<>();
        }
        return assignments;
    }
}
