package org.proticity.bttv.client;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Indicates a failure to process a BTTV API response.
 */
@ParametersAreNonnullByDefault
public class BttvApiException extends RuntimeException {
    private static final long serialVersionUID = 0;

    /**
     * Instantiates a new {@link BttvApiException} with a custom message.
     *
     * @param message the error message.
     */
    public BttvApiException(String message) {
        super(message);
    }

    /**
     * Instantiates a new {@link BttvApiException} with a custom message and based on another exception.
     *
     * @param message the error message.
     * @param cause the exception causing the API exception.
     */
    public BttvApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
