package org.proticity.bttv.client;

import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

@JsonIgnoreProperties(ignoreUnknown = true)
@ParametersAreNonnullByDefault
class PubSubMessage {
    @JsonProperty("name")
    String name;

    @JsonProperty("data")
    JsonNode data;

    PubSubMessage() {
    }

    PubSubMessage(String name) {
        this.name = name;
    }
}
