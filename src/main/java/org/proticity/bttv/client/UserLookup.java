package org.proticity.bttv.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Provides data on a user lookup from the BTTV PubSub API.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class UserLookup implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * A type reference for an emote.
     */
    private static final TypeReference<Emote> EMOTE_TYPE_REFERENCE = new TypeReference<>() { };

    /**
     * The name of the channel the user's information was broadcast for.
     */
    String channel;

    /**
     * The information about the user in the channel.
     */
    User user;

    /**
     * Constructs a {@link UserLookup}.
     */
    protected UserLookup() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserLookup that = (UserLookup) o;
        return channel.equals(that.channel) && user.equals(that.user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(channel, user);
    }

    /**
     * Returns the name of the channel the user's information was broadcast for.
     *
     * @return The name of the channel the user's information was broadcast for.
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Returns the information about the user in the channel.
     *
     * @return The information about the user in the channel.
     */
    public User getUser() {
        return user;
    }

    /**
     * A deserializer for user lookup messages.
     */
    static class UserLookupDeserializer extends StdDeserializer<UserLookup> {
        private static final long serialVersionUID = 0;

        /**
         * Instantiate a new {@link UserLookupDeserializer}.
         */
        UserLookupDeserializer() {
            this(null);
        }

        /**
         * Constructs a new deserializer.
         */
        UserLookupDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public UserLookup deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var lookup = new UserLookup();
            lookup.user = new User();

            var dataNode = rootNode.get("data");
            lookup.channel = dataNode.get("channel").textValue();
            lookup.user.subscribed = dataNode.get("subscribed").booleanValue();
            lookup.user.pro = dataNode.get("pro").booleanValue();
            lookup.user.glowing = dataNode.get("glow").booleanValue();
            lookup.user.username = dataNode.get("name").textValue();

            var emoteNodes = dataNode.get("emotes");
            if (emoteNodes != null) {
                var emotes = lookup.user.initEmotes(emoteNodes.size());
                for (var emoteNode : emoteNodes) {
                    var subParser = emoteNode.traverse(jsonParser.getCodec());
                    Emote emote = subParser.readValueAs(EMOTE_TYPE_REFERENCE);
                    emotes.put(emote.name, emote);
                }
            } else {
                lookup.user.initEmotes(0);
            }

            return lookup;
        }
    }
}
