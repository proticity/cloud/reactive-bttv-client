/**
 * Contains a client for the Better Twitch TV service, allowing emote and badge resolution for BTTV resources with
 * Twitch chat.
 */
package org.proticity.bttv.client;
