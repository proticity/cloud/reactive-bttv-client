package org.proticity.bttv.client;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Represents information about a user's BTTV service level and personal emotes.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class User implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * Whether the user is a legacy BTTV subscriber.
     */
    boolean subscribed;

    /**
     * Whether the user is a BTTV Pro subscriber.
     */
    boolean pro;

    /**
     * Whether the user has "glow" from legacy BTTV subscription.
     */
    boolean glowing;

    /**
     * The username of the user.
     */
    String username;

    /**
     * A mapping of the user's personal emotes codes to the emote information.
     */
    private Map<String, Emote> emotes;

    /**
     * Constructs a new {@link User}.
     */
    protected User() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return subscribed == user.subscribed &&
                pro == user.pro &&
                glowing == user.glowing &&
                Objects.equals(username, user.username) &&
                Objects.equals(emotes, user.emotes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(subscribed, pro, glowing, username, emotes);
    }

    /**
     * Initialize the emotes map.
     *
     * @param capacity the capacity of the emotes map.
     *
     * @return The new emotes map.
     */
    Map<String, Emote> initEmotes(final int capacity) {
        emotes = new HashMap<>(capacity);
        return emotes;
    }

    /**
     * Returns whether the user is a legacy BTTV subscriber.
     *
     * @return Whether the user is a legacy BTTV subscriber.
     */
    @JsonGetter("isSubscribed")
    public boolean isSubscribed() {
        return subscribed;
    }

    /**
     * Returns whether the user is a BTTV Pro subscriber.
     *
     * @return Whether the user is a BTTV Pro subscriber.
     */
    @JsonGetter("isPro")
    public boolean isPro() {
        return pro;
    }

    /**
     * Returns whether the user is a legacy BTTV subscriber.
     *
     * @return Whether the user is a legacy BTTV subscriber.
     */
    @JsonGetter("hasGlow")
    public boolean isGlowing() {
        return glowing;
    }

    /**
     * Returns the username of the user.
     *
     * @return The username of the user.
     */
    @JsonGetter("username")
    public String getUsername() {
        return username;
    }

    /**
     * Returns a mapping of the user's personal emotes codes to the emote information.
     *
     * @return A mapping of the user's personal emotes codes to the emote information.
     */
    @JsonGetter("emotes")
    public Map<String, Emote> getEmotes() {
        return emotes;
    }
}
