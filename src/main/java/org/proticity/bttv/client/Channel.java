package org.proticity.bttv.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Provides information about a channel's settings and contents in BTTV's service.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ParametersAreNonnullByDefault
public class Channel implements Serializable {
    private static final long serialVersionUID = 0;

    /**
     * A type reference for a set of strings.
     */
    private static final TypeReference<Set<String>> BOTS_TYPE_REFERENCE = new TypeReference<>() { };

    /**
     * A type reference for the {@link Emote} type.
     */
    private static final TypeReference<Emote> EMOTE_TYPE_REFERENCE = new TypeReference<>() { };

    /**
     * A set of usernames which are known to BTTV to be bots.
     */
    private Set<String> bots;

    /**
     * A map with the channel's BTTV emotes, using the emote's name (code) as the key.
     */
    private Map<String, Emote> emotes;

    /**
     * The name of the channel.
     */
    String name;

    /**
     * Constructs a new {@link Channel}.
     */
    protected Channel() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Channel channel = (Channel) o;
        return bots.equals(channel.bots) &&
                emotes.equals(channel.emotes) &&
                name.equals(channel.name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(bots, emotes, name);
    }

    /**
     * Returns a set of usernames which are known to BTTV to be bots.
     *
     * @return A set of usernames which are known to BTTV to be bots.
     */
    @JsonGetter("bots")
    public Set<String> getBots() {
        return bots;
    }

    /**
     * Returns a map with the channel's BTTV emotes, using the emote's name (code) as the key.
     *
     * @return A map with the channel's BTTV emotes, using the emote's name (code) as the key.
     */
    @JsonGetter("emotes")
    public Map<String, Emote> getEmotes() {
        return emotes;
    }

    /**
     * Returns the name of the channel.
     *
     * @return The name of the channel.
     */
    @JsonGetter("name")
    public String getName() {
        return name;
    }

    /**
     * A deserializer for channel information.
     */
    static class ChannelDeserializer extends StdDeserializer<Channel> {
        private static final long serialVersionUID = 0;

        /**
         * Construct a default {@link ChannelDeserializer}.
         */
        ChannelDeserializer() {
            this(null);
        }

        /**
         * Constructs a new deserializer.
         */
        ChannelDeserializer(@Nullable Class<?> valueClass) {
            super(valueClass);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Channel deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException {
            var rootNode = jsonParser.getCodec().<JsonNode>readTree(jsonParser);
            var channel = new Channel();

            var subParser = rootNode.get("bots").traverse(jsonParser.getCodec());
            channel.bots = subParser.readValueAs(BOTS_TYPE_REFERENCE);

            var emotesNode = rootNode.get("emotes");
            channel.emotes = new HashMap<>(emotesNode.size());
            for (var emoteNode : emotesNode) {
                subParser = emoteNode.traverse(jsonParser.getCodec());
                Emote emote = subParser.readValueAs(EMOTE_TYPE_REFERENCE);
                channel.emotes.put(emote.getName(), emote);
            }

            return channel;
        }
    }
}
