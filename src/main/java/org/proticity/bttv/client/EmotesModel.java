package org.proticity.bttv.client;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
class EmotesModel {
    @JsonProperty("emotes")
    List<Emote> emotes;

    @JsonProperty("urlTemplate")
    String urlTemplate;

    EmotesModel() {
    }
}
