package org.proticity.bttv.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.netty.http.HttpResources;

import java.util.UUID;

public class EmoteIT {
    private static BttvClient client;

    @BeforeAll
    public static void setup() {
        client = BttvClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .connect();
    }

    @AfterAll
    public static void dispose() {
        client.close();
    }

    @Test
    public void testGlobalEmotes() {
        var globalEmotes = client.globalEmoteSet().collectMap(Emote::getName).block();
        Assertions.assertNotNull(globalEmotes);
        Assertions.assertTrue(globalEmotes.size() > 1);

        var emote = globalEmotes.get("FeelsBadMan");
        Assertions.assertNotNull(emote);
        Assertions.assertEquals("FeelsBadMan", emote.getName());
        Assertions.assertEquals("566c9fc265dbbdab32ec053b", emote.getId());
        Assertions.assertTrue(emote.getOwner().isEmpty());
        Assertions.assertEquals("png", emote.getImageType());
        Assertions.assertTrue(emote.isGlobal());
        Assertions.assertEquals("https://cdn.betterttv.net/emote/566c9fc265dbbdab32ec053b/1x", emote.getUrl());
        Assertions.assertEquals("https://cdn.betterttv.net/emote/566c9fc265dbbdab32ec053b/1x", emote.getUrl(1));
        Assertions.assertEquals("https://cdn.betterttv.net/emote/566c9fc265dbbdab32ec053b/2x", emote.getUrl(2));
        Assertions.assertEquals("https://cdn.betterttv.net/emote/566c9fc265dbbdab32ec053b/3x", emote.getUrl(3));
        Assertions.assertEquals("https://cdn.betterttv.net/emote/566c9fc265dbbdab32ec053b/3x", emote.getUrl(4));
        try {
            emote.getUrl(0);
            Assertions.fail("Emote returned a URL for the invalid size multiplier of 0.");
        } catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testChannelEmotes() {
        var channel = client.channel("charmedbaryon").block();
        Assertions.assertEquals("charmedbaryon", channel.getName());
        Assertions.assertTrue(channel.getBots().contains("baryonbot"));

        var emote = channel.getEmotes().get("baryonLewd");
        Assertions.assertEquals("baryonLewd", emote.getName());
        Assertions.assertEquals("png", emote.getImageType());
        Assertions.assertTrue(emote.getOwner().isPresent());
        Assertions.assertEquals("charmedbaryon", emote.getOwner().get());
        Assertions.assertFalse(emote.isGlobal());
    }

    @Test
    public void testInvalidChannel() {
        Assertions.assertThrows(BttvApiException.class, () -> client.channel(UUID.randomUUID().toString()).block());
    }
}
