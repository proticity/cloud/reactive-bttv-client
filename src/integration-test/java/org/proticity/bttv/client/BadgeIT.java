package org.proticity.bttv.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.netty.http.HttpResources;

public class BadgeIT {
    private static BttvClient client;

    @BeforeAll
    public static void setup() {
        client = BttvClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .connect();
    }

    @AfterAll
    public static void dispose() {
        client.close();
    }

    @Test
    public void testBadges() {
        var badgeInfo = client.badges().block();
        Assertions.assertNotNull(badgeInfo);
        Assertions.assertTrue(badgeInfo.getBadges().size() > 0);
        for (var assignmentEntry : badgeInfo.getBadgeAssignments().entrySet()) {
            for (var badgeEntry : assignmentEntry.getValue().entrySet()) {
                Assertions.assertSame(badgeEntry.getValue(), badgeInfo.getBadges().get(badgeEntry.getKey()));
            }
        }
        var badge = badgeInfo.getBadges().get("developer");
        Assertions.assertEquals("NightDev Developer", badge.getDescription());
        Assertions.assertNotNull(badge.getUrl());
    }
}
