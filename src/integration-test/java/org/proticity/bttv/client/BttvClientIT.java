package org.proticity.bttv.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.handler.ssl.SslContextBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.netty.http.HttpResources;

import javax.net.ssl.SSLException;

public class BttvClientIT {
    @Test
    public void testDispose() {
        var client = BttvClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .secure(builder -> {
                    // Test that it works fine with the custom SSL context.
                    try {
                        builder.sslContext(SslContextBuilder.forClient().build());
                    } catch (SSLException e) {
                        throw new RuntimeException(e);
                    }
                })
                .connect();

        client.subscribeChannel("twitchpresents").block();
        client.disposeNow();
        Assertions.assertThrows(RuntimeException.class, () -> client.unsubscribeChannel("twitchpresents").block());
    }
}
