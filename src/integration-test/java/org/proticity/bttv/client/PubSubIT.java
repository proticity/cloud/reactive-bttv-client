package org.proticity.bttv.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.netty.http.HttpResources;

import java.time.Duration;

public class PubSubIT {
    private static BttvClient client;

    @BeforeAll
    public static void setup() {
        client = BttvClient.create()
                .connectionProvider(HttpResources.get())
                .objectMapper(new ObjectMapper())
                .secure()
                .connect();
    }

    @AfterAll
    public static void dispose() {
        client.close();
    }

    @Test
    public void testPubSub() throws Exception {
        client.subscribeChannel("charmedbaryon").block();
        client.broadcast("charmedbaryon", "charmedbaryon").block();

        var lookup = client.users().next().block(Duration.ofSeconds(3));
        Assertions.assertEquals("charmedbaryon", lookup.getChannel());
        Assertions.assertEquals("charmedbaryon", lookup.getUser().getUsername());
        Assertions.assertTrue(lookup.getUser().isPro());
        Assertions.assertFalse(lookup.getUser().isSubscribed());
        Assertions.assertFalse(lookup.getUser().isGlowing());
        Assertions.assertTrue(lookup.getUser().getEmotes().size() > 0);
        for (var emoteEntry : lookup.getUser().getEmotes().entrySet()) {
            Assertions.assertEquals(emoteEntry.getKey(), emoteEntry.getValue().getName());
        }

        client.unsubscribeChannel("charmedbaryon").block();
        client.broadcast("charmedbaryon", "charmedbaryon").block();

        try {
            //lookup = connectable.next().block(Duration.ofSeconds(1));
            lookup = client.users().next().block(Duration.ofSeconds(3));
            if (lookup == null) {
                return;
            }
            Assertions.assertNotEquals("charmedbaryon", lookup.getUser().getUsername());
        } catch (IllegalStateException e) {
        }
    }
}
