package org.proticity.bttv.client;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

public class BttvClientTest {
    private DisposableServer server;
    private BttvClient client;

    @AfterEach
    public void disposeServer() {
        if (server != null) {
            server.disposeNow();
            server = null;
        }
        if (client != null) {
            client.disposeNow();
            client = null;
        }
    }

    @Test
    public void testJsonInvalid() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) ->
                        res.sendString(Mono.just("{foo}")))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(BttvApiException.class, () -> client.badges().block());
    }

    @Test
    public void testHandleNotFound() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) ->
                        res.status(404).sendString(Mono.just("{\"status\":404}")))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(BttvApiException.class, () -> client.channel("nonChannel").block());
    }

    @Test
    public void testDisposeNow() {
        var client = BttvClient.create().connect();
        Exception error = null;
        try {
            client.disposeNow();
        } catch (Exception e) {
            error = e;
        }
        Assertions.assertNull(error);
    }
}
