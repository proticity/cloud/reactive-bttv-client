package org.proticity.bttv.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

import java.io.IOException;

public class EmoteTest {
    private DisposableServer server;
    private BttvClient client;

    @AfterEach
    public void disposeServer() {
        if (server != null) {
            server.disposeNow();
            server = null;
        }
        if (client != null) {
            client.disposeNow();
            client = null;
        }
    }

    @Test
    public void testGlobalEmotes() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) ->
                        res.sendString(Mono.just("{\"urlTemplate\":\"//{{id}}/{{image}}\",\"emotes\":[{\"id\":\"abc\"," +
                                "\"code\":\"MyTest\",\"imageType\":\"png\",\"url\":\"//abc/1x\"}," +
                                "{\"id\":\"def\",\"code\":\"OtherTest\",\"imageType\":\"png\",\"url\":\"//def/1x\"}]}")))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var emotes = client.globalEmoteSet().collectMap(Emote::getName).block();
        Assertions.assertNotNull(emotes);
        var emote = emotes.get("MyTest");
        Assertions.assertEquals("abc", emote.getId());
        Assertions.assertEquals("MyTest", emote.getName());
        Assertions.assertTrue(emote.getOwner().isEmpty());
        Assertions.assertTrue(emote.isGlobal());
        Assertions.assertEquals("png", emote.getImageType());
        Assertions.assertEquals("https://abc/1x", emote.getUrl());
        Assertions.assertEquals("https://abc/2x", emote.getUrl(2));
        Assertions.assertEquals("https://abc/3x", emote.getUrl(3));
        Assertions.assertEquals("https://abc/3x", emote.getUrl(4));
        Assertions.assertThrows(IllegalArgumentException.class, () -> emote.getUrl(0));
        var otherTest = emotes.get("OtherTest");
        Assertions.assertEquals("def", otherTest.getId());
        Assertions.assertEquals("OtherTest", otherTest.getName());
        Assertions.assertTrue(emote.getOwner().isEmpty());
        Assertions.assertTrue(emote.isGlobal());
        Assertions.assertEquals("https://def/1x", otherTest.getUrl());
        Assertions.assertEquals("https://def/2x", otherTest.getUrl(2));
        Assertions.assertEquals("https://def/3x", otherTest.getUrl(3));
        Assertions.assertEquals("https://def/3x", otherTest.getUrl(4));
    }

    @Test
    public void testChannelEmotes() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) ->
                        res.sendString(Mono.just("{\"bots\":[\"mybot\"],\"emotes\":[{\"id\":\"abc\"," +
                                "\"code\":\"MyTest\",\"channel\":\"otherChan\",\"imageType\":\"png\"," +
                                "\"url\":\"//1x\",\"urlTemplate\":\"//{{image}}\"}]}")))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var channel = client.channel("mychannel").block();
        Assertions.assertNotNull(channel);
        Assertions.assertEquals("mychannel", channel.getName());
        Assertions.assertTrue(channel.getBots().contains("mybot"));
        var emote = channel.getEmotes().get("MyTest");
        Assertions.assertNotNull(emote);
        Assertions.assertEquals("MyTest", emote.getName());
        Assertions.assertEquals("otherChan", emote.getOwner().get());
        Assertions.assertFalse(emote.isGlobal());
        Assertions.assertEquals("png", emote.getImageType());
        Assertions.assertEquals("https://1x", emote.getUrl());
        Assertions.assertEquals("https://2x", emote.getUrl(2));
        Assertions.assertEquals("https://3x", emote.getUrl(3));
        Assertions.assertEquals("https://3x", emote.getUrl(4));
        Assertions.assertThrows(IllegalArgumentException.class, () -> emote.getUrl(0));
    }

    @Test
    public void testChannelSerialization() throws IOException {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) ->
                        res.sendString(Mono.just("{\"bots\":[\"mybot\"],\"emotes\":[{\"id\":\"abc\"," +
                                "\"code\":\"MyTest\",\"channel\":\"otherChan\",\"imageType\":\"png\"," +
                                "\"url\":\"//1x\",\"urlTemplate\":\"//{{image}}\"}]}")))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var originalChannel = client.channel("mychannel").block();
        Assertions.assertNotNull(originalChannel);

        var mapper = new ObjectMapper().registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        var serialized = mapper.writeValueAsString(originalChannel);
        var channel = mapper.readValue(serialized, Channel.class);
        Assertions.assertEquals(originalChannel, channel);
        Assertions.assertEquals(originalChannel.hashCode(), channel.hashCode());

        Assertions.assertEquals("mychannel", channel.getName());
        Assertions.assertTrue(channel.getBots().contains("mybot"));
        var emote = channel.getEmotes().get("MyTest");
        Assertions.assertNotNull(emote);
        Assertions.assertEquals("MyTest", emote.getName());
        Assertions.assertEquals("otherChan", emote.getOwner().get());
        Assertions.assertFalse(emote.isGlobal());
        Assertions.assertEquals("png", emote.getImageType());
        Assertions.assertEquals("https://1x", emote.getUrl());
        Assertions.assertEquals("https://2x", emote.getUrl(2));
        Assertions.assertEquals("https://3x", emote.getUrl(3));
        Assertions.assertEquals("https://3x", emote.getUrl(4));
        Assertions.assertThrows(IllegalArgumentException.class, () -> emote.getUrl(0));
    }

    @Test
    public void testSameEquality() {
        var emote = new Emote();
        Assertions.assertEquals(emote, emote);
    }

    @Test
    public void testNullEquality() {
        var emote = new Emote();
        Assertions.assertFalse(emote.equals(null));
    }

    @Test
    public void testClassEquality() {
        var emote = new Emote();
        Assertions.assertNotEquals(0, emote);
    }
}
