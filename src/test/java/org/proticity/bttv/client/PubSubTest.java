package org.proticity.bttv.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class PubSubTest {
    private DisposableServer server;
    private BttvClient client;

    @AfterEach
    public void disposeServer() {
        if (server != null) {
            server.disposeNow();
            server = null;
        }
        if (client != null) {
            client.disposeNow();
            client = null;
        }
    }

    @Test
    public void testPubSubLookupUserNonPro() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> res.sendWebsocket((in, out) -> {
                    return out.sendString(Mono.just("{\"name\":\"lookup_user\",\"data\":" +
                            "{\"name\":\"username\",\"channel\":\"chan\",\"pro\":false,\"subscribed\":false,\"glow\":false}}"));
                }))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .pubSubEndpoint("http://localhost:" + server.port())
                .connect();

        var lookup = client.users().blockFirst();
        Assertions.assertNotNull(lookup);
        Assertions.assertEquals("chan", lookup.getChannel());
        Assertions.assertEquals("username", lookup.getUser().getUsername());
        Assertions.assertFalse(lookup.getUser().isPro());
        Assertions.assertFalse(lookup.getUser().isSubscribed());
        Assertions.assertFalse(lookup.getUser().isGlowing());
        Assertions.assertTrue(lookup.getUser().getEmotes().size() == 0);
    }

    @Test
    public void testPubSubLookupUserPro() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> res.sendWebsocket((in, out) -> {
                    return out.sendString(Mono.just("{\"name\":\"lookup_user\",\"data\":" +
                            "{\"name\":\"username\",\"channel\":\"chan\",\"pro\":true,\"subscribed\":false,\"glow\":false," +
                            "\"emotes\":[{\"id\":\"abc\",\"code\":\"MyTest\",\"channel\":\"otherChan\",\"imageType\":\"png\"," +
                            "\"url\":\"//1x\",\"urlTemplate\":\"//{{image}}\"}]}}"));
                }))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .pubSubEndpoint("http://localhost:" + server.port())
                .connect();

        var lookup = client.users().blockFirst();
        Assertions.assertNotNull(lookup);
        Assertions.assertEquals("chan", lookup.getChannel());
        Assertions.assertEquals("username", lookup.getUser().getUsername());
        Assertions.assertTrue(lookup.getUser().isPro());
        Assertions.assertFalse(lookup.getUser().isSubscribed());
        Assertions.assertFalse(lookup.getUser().isGlowing());
        Assertions.assertEquals(1, lookup.getUser().getEmotes().size());

        var emote = lookup.getUser().getEmotes().get("MyTest");
        Assertions.assertNotNull(emote);
        Assertions.assertEquals("MyTest", emote.getName());
        Assertions.assertEquals("otherChan", emote.getOwner().get());
        Assertions.assertEquals("png", emote.getImageType());
        Assertions.assertEquals("https://1x", emote.getUrl());
        Assertions.assertEquals("https://2x", emote.getUrl(2));
        Assertions.assertEquals("https://3x", emote.getUrl(3));
        Assertions.assertEquals("https://3x", emote.getUrl(4));
        Assertions.assertThrows(IllegalArgumentException.class, () -> emote.getUrl(0));
    }

    @Test
    public void testUserLookupSerialization() throws IOException {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> res.sendWebsocket((in, out) -> {
                    return out.sendString(Mono.just("{\"name\":\"lookup_user\",\"data\":" +
                            "{\"name\":\"username\",\"channel\":\"chan\",\"pro\":true,\"subscribed\":false,\"glow\":false," +
                            "\"emotes\":[{\"id\":\"abc\",\"code\":\"MyTest\",\"channel\":\"otherChan\",\"imageType\":\"png\"," +
                            "\"url\":\"//1x\",\"urlTemplate\":\"//{{image}}\"}]}}"));
                }))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .pubSubEndpoint("http://localhost:" + server.port())
                .connect();

        var originalLookup = client.users().blockFirst();
        Assertions.assertNotNull(originalLookup);

        var mapper = new ObjectMapper().registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        var serialized = mapper.writeValueAsString(originalLookup);
        var lookup = mapper.readValue(serialized, UserLookup.class);
        Assertions.assertEquals(originalLookup, lookup);
        Assertions.assertEquals(originalLookup.hashCode(), lookup.hashCode());

        Assertions.assertEquals("chan", lookup.getChannel());
        Assertions.assertEquals("username", lookup.getUser().getUsername());
        Assertions.assertTrue(lookup.getUser().isPro());
        Assertions.assertFalse(lookup.getUser().isSubscribed());
        Assertions.assertFalse(lookup.getUser().isGlowing());
        Assertions.assertEquals(1, lookup.getUser().getEmotes().size());

        var emote = lookup.getUser().getEmotes().get("MyTest");
        Assertions.assertNotNull(emote);
        Assertions.assertEquals("MyTest", emote.getName());
        Assertions.assertEquals("otherChan", emote.getOwner().get());
        Assertions.assertEquals("png", emote.getImageType());
        Assertions.assertEquals("https://1x", emote.getUrl());
        Assertions.assertEquals("https://2x", emote.getUrl(2));
        Assertions.assertEquals("https://3x", emote.getUrl(3));
        Assertions.assertEquals("https://3x", emote.getUrl(4));
        Assertions.assertThrows(IllegalArgumentException.class, () -> emote.getUrl(0));
    }

    @Test
    public void testPubSubBroadcast() throws Exception {
        var latch = new CountDownLatch(1);

        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> res.sendWebsocket((in, out) -> {
                    return in.receiveFrames()
                            .filter(frame -> frame instanceof TextWebSocketFrame)
                            .cast(TextWebSocketFrame.class)
                            .map(TextWebSocketFrame::text)
                            .doOnNext(text -> {
                                Assertions.assertEquals("{\"name\":\"broadcast_me\",\"data\":" +
                                        "{\"name\":\"myname\",\"channel\":\"MyChannel\"}}", text);
                                latch.countDown();
                            }).then();

                }))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .pubSubEndpoint("http://localhost:" + server.port())
                .connect();

        client.broadcast("MyChannel", "MyName").block();
        Assertions.assertTrue(latch.await(1, TimeUnit.SECONDS));
    }

    @Test
    public void testPubSubSubscribe() throws Exception {
        var latch = new CountDownLatch(1);

        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> res.sendWebsocket((in, out) -> {
                    return in.receiveFrames()
                            .filter(frame -> frame instanceof TextWebSocketFrame)
                            .cast(TextWebSocketFrame.class)
                            .map(TextWebSocketFrame::text)
                            .doOnNext(text -> {
                                Assertions.assertEquals("{\"name\":\"join_channel\",\"data\":{\"name\":\"MyChannel\"}}", text);
                                latch.countDown();
                            }).then();

                }))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .pubSubEndpoint("http://localhost:" + server.port())
                .connect();

        client.subscribeChannel("MyChannel").block();
        Assertions.assertTrue(latch.await(1, TimeUnit.SECONDS));
    }

    @Test
    public void testPubSubUnsubscribe() throws Exception{
        var latch = new CountDownLatch(1);

        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> res.sendWebsocket((in, out) -> {
                    return in.receiveFrames()
                            .filter(frame -> frame instanceof TextWebSocketFrame)
                            .cast(TextWebSocketFrame.class)
                            .map(TextWebSocketFrame::text)
                            .doOnNext(text -> {
                                Assertions.assertEquals("{\"name\":\"part_channel\",\"data\":{\"name\":\"MyChannel\"}}", text);
                                latch.countDown();
                            }).then();

                }))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .pubSubEndpoint("http://localhost:" + server.port())
                .connect();

        client.unsubscribeChannel("MyChannel").block();
        Assertions.assertTrue(latch.await(1, TimeUnit.SECONDS));
    }

    @Test
    public void testPubSubInvalidJson() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> res.sendWebsocket((in, out) -> {
                    return out.sendString(Mono.just("{foo}"));
                }))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .pubSubEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertThrows(BttvApiException.class, () -> client.users().next().block());
    }

    @Test
    public void testPubSubUnknownMessageType() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) -> res.sendWebsocket((in, out) -> {
                    return out.sendString(Mono.just("{\"name\":\"unknown\",\"data\":{}}"));
                }))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .pubSubEndpoint("http://localhost:" + server.port())
                .connect();

        Assertions.assertNull(client.users().next().block());
    }

    @Test
    public void testChannelSameEquality() {
        var channel = new Channel();
        Assertions.assertEquals(channel, channel);
    }

    @Test
    public void testChannelNullEquality() {
        var channel = new Channel();
        Assertions.assertFalse(channel.equals(null));
    }

    @Test
    public void testChannelClassEquality() {
        var channel = new Channel();
        Assertions.assertNotEquals(0, channel);
    }

    @Test
    public void testUserSameEquality() {
        var user = new User();
        Assertions.assertEquals(user, user);
    }

    @Test
    public void testUserNullEquality() {
        var user = new User();
        Assertions.assertFalse(user.equals(null));
    }

    @Test
    public void testUserClassEquality() {
        var user = new User();
        Assertions.assertNotEquals(0, user);
    }

    @Test
    public void testUserLookupSameEquality() {
        var lookup = new UserLookup();
        Assertions.assertEquals(lookup, lookup);
    }

    @Test
    public void testUserLookupNullEquality() {
        var lookup = new UserLookup();
        Assertions.assertFalse(lookup.equals(null));
    }

    @Test
    public void testUserLookupClassEquality() {
        var lookup = new UserLookup();
        Assertions.assertNotEquals(0, lookup);
    }
}
