package org.proticity.bttv.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

import java.io.IOException;

public class BadgeTest {
    private DisposableServer server;
    private BttvClient client;

    @AfterEach
    public void disposeServer() {
        if (server != null) {
            server.disposeNow();
            server = null;
        }
        if (client != null) {
            client.disposeNow();
            client = null;
        }
    }

    @Test
    public void testBadges() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) ->
                        res.sendString(Mono.just(
                                "{\"types\":[" +
                                        "{\"name\":\"dev\",\"description\":\"Dev\",\"svg\":\"dev.svg\"}," +
                                        "{\"name\":\"mod\",\"description\":\"Mod\",\"svg\":\"mod.svg\"}" +
                                        "],\"badges\":[" +
                                        "{\"name\":\"user\",\"type\":\"dev\"}," +
                                        "{\"name\":\"user2\",\"type\":\"dev\"}," +
                                        "{\"name\":\"user\",\"type\":\"mod\"}" +
                                        "]}")))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var badgeInfo = client.badges().block();
        Assertions.assertNotNull(badgeInfo);
        Assertions.assertEquals(2, badgeInfo.getBadges().size());
        Assertions.assertEquals(2, badgeInfo.getBadgeAssignments().size());
        var devBadge = badgeInfo.getBadges().get("dev");
        Assertions.assertEquals("dev", devBadge.getName());
        Assertions.assertEquals("Dev", devBadge.getDescription());
        Assertions.assertEquals("dev.svg", devBadge.getUrl());
        var modBadge = badgeInfo.getBadges().get("mod");
        Assertions.assertEquals("mod", modBadge.getName());
        Assertions.assertEquals("Mod", modBadge.getDescription());
        Assertions.assertEquals("mod.svg", modBadge.getUrl());
    }

    @Test
    public void testBadgesSerialization() throws IOException {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) ->
                        res.sendString(Mono.just(
                                "{\"types\":[" +
                                        "{\"name\":\"dev\",\"description\":\"Dev\",\"svg\":\"dev.svg\"}," +
                                        "{\"name\":\"mod\",\"description\":\"Mod\",\"svg\":\"mod.svg\"}" +
                                        "],\"badges\":[" +
                                        "{\"name\":\"user\",\"type\":\"dev\"}," +
                                        "{\"name\":\"user2\",\"type\":\"dev\"}," +
                                        "{\"name\":\"user\",\"type\":\"mod\"}" +
                                        "]}")))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var originalBadgeInfo = client.badges().block();
        Assertions.assertNotNull(originalBadgeInfo);

        var mapper = new ObjectMapper().registerModule(new Jdk8Module().configureAbsentsAsNulls(true));
        var serialized = mapper.writeValueAsString(originalBadgeInfo);
        var badgeInfo = mapper.readValue(serialized, BadgeCollection.class);
        Assertions.assertEquals(originalBadgeInfo, badgeInfo);
        Assertions.assertEquals(originalBadgeInfo.hashCode(), badgeInfo.hashCode());

        Assertions.assertEquals(2, badgeInfo.getBadges().size());
        Assertions.assertEquals(2, badgeInfo.getBadgeAssignments().size());
        var devBadge = badgeInfo.getBadges().get("dev");
        Assertions.assertEquals("dev", devBadge.getName());
        Assertions.assertEquals("Dev", devBadge.getDescription());
        Assertions.assertEquals("dev.svg", devBadge.getUrl());
        var modBadge = badgeInfo.getBadges().get("mod");
        Assertions.assertEquals("mod", modBadge.getName());
        Assertions.assertEquals("Mod", modBadge.getDescription());
        Assertions.assertEquals("mod.svg", modBadge.getUrl());
    }

    @Test
    public void testInvalidBadgeReference() {
        server = HttpServer.create()
                .host("0.0.0.0")
                .port(0)
                .handle((req, res) ->
                        res.sendString(Mono.just("{\"types\":[],\"badges\":[{\"name\":\"user\",\"type\":\"dev\"}]}")))
                .bindNow();

        client = BttvClient.create()
                .secure(false)
                .restEndpoint("http://localhost:" + server.port())
                .connect();

        var badgeInfo = client.badges().block();
        Assertions.assertNotNull(badgeInfo);
        Assertions.assertEquals(0, badgeInfo.getBadges().size());
        Assertions.assertEquals(0, badgeInfo.getBadgeAssignments().size());
    }

    @Test
    public void testBadgeSameEquality() {
        var badge = new Badge();
        Assertions.assertEquals(badge, badge);
    }

    @Test
    public void testBadgeNullEquality() {
        var badge = new Badge();
        Assertions.assertFalse(badge.equals(null));
    }

    @Test
    public void testBadgeClassEquality() {
        var badge = new Badge();
        Assertions.assertNotEquals(0, badge);
    }

    @Test
    public void testBadgeCollectionSameEquality() {
        var collection = new BadgeCollection();
        Assertions.assertEquals(collection, collection);
    }

    @Test
    public void testBadgeCollectionNullEquality() {
        var collection = new BadgeCollection();
        Assertions.assertFalse(collection.equals(null));
    }

    @Test
    public void testBadgeCollectionClassEquality() {
        var collection = new BadgeCollection();
        Assertions.assertNotEquals(0, collection);
    }
}
